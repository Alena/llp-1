section .data
	buf : times 256 db 0

section .text
 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax ; здесь и далее обнуление аккум.
.loop:
	cmp byte[rdi + rax], 0  ; сравниваем с нулем			
	je .end ; если да, заканчиваем счет	
	inc rax	; либо инкрементим счетчик 			
	jmp .loop
.end:
	ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length 
    mov rsi, rdi ; строка для вывода
    mov rdi, 1 ; вывод в stdout
    mov rdx, rax ; длина строки для счетчика
    mov rax, 1 ; код write
    syscall
    xor rax, rax    
    ret



; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rsi, rsp ; символ для вывода
    mov rdi, 1 ; вывод в stdout
    mov rdx, 1 ; длина 1 символ 
    mov rax, 1 ; код write
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10 ; конец строки 
    call print_char
    xor rax, rax  
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r11, rsp ; в r11 адрес вершины стека 
    mov rax, rdi ; сохраняем число в аккум
    mov rdi, 10 ; делитель в rdi
    sub rsp, 256 ; выделяем место в стеке
    dec r11 ; выделяем место под 0 на вершине стэка. далее нультерм для вывода 
    mov byte[r11], 0 ; кладем 0 на вершину стека
.loop:
    dec r11 ; место для следующего символа 
    xor rdx, rdx ; обнуление rdx
    div rdi ; целочисленное деление на 10, остаток -> в rdx
    add rdx, '0' ; код символа ASCII
    mov byte[r11], dl; сохраняем символ на стек
    test rax, rax; проверяем на равенство 0
    jnz .loop ; если rax <> 0, продолжаем
    mov rdi, r11 ; аргумент для print_string
    call print_string
    add rsp, 256 
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi ; определение флагов 
    jns print_uint; >=0 печатаем
    push rdi; кладем на стек 
    mov rdi, '-'; запись в rdi
    call print_char; печатаем минус
    pop rdi 
    neg rdi; меняем знак на +
    jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx  
    xor rax, rax
    xor rdx, rdx
.loop:    
    mov al, byte[rsi + rcx] ; копируем 1 символ 1 строки
    mov dl, byte[rdi + rcx] ; копируем 1 символ 2 строки
    cmp al, dl ; сравниваем символы
    jne .exit ; выход (не равны)
    test al, dl ; проверка на окончание строки 
    jz .done ; выход
    inc rcx 
    jmp .loop
.exit:
    mov rax, 0
    ret
.done:
    mov rax, 1
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp ; уменьшаем rsp для места под символ
    mov rax, 0 ; код системного вызова read
    mov rdx, 1 ; читаем символ
    mov rdi, 0 ; stdin
    mov rsi, rsp ; кладем на стек 
    syscall
    mov rax, [rsp] ; сохраняем в rax
    inc rsp ; инкрементим rsp
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx
    mov r8, rsi ; сохраняем размер буфера
    mov r9, rdi ; сохраняем адрес буфера
    xor rbx, rbx ; обнуление счетчика длины
    xor rdi, rdi ; ввод - stdin
    mov rdx, 1 ; читаем 1 символ
.skip:
    xor rax, rax ; код read
    mov rsi, buf ; заносим адрес для ситвания
    syscall
    cmp al, 0 ; сравниваем с концом строки
    je .finally ; если равно, выход
    cmp byte[buf], 0x21 ; сравниваем с кодом последнего символа
    jb .skip ; если меньше, пропускаем его
    inc rbx ; либо добавляем
.read:
    xor rax, rax ; код read
    lea rsi, [buf + rbx] ; адрес, куда читаем, в rsi
    syscall
    cmp byte [buf + rbx], 0x21 ; сравниваем c последним символом
    jb .finally ; если меньше, выход
    cmp r8, rbx ; либо проверяем на размер
    jbe .exit ; если символ не помещается, возвращаем 0
    inc rbx ; либо добавляем
    jmp .read ; читаем следующий
.finally:
    mov byte[buf + rbx], 0 ; нультермируем строку в буфере
    mov rdx, rbx ; длина строки
    mov rax, buf ; указатель на буфер
    pop rbx ; восстанавливаем rbx
    ret
.exit:
    xor rdx, r8 ; записываем длину буфера, т.е. кол-во символов
    xor rax, rax
    pop rbx ; восстанавливаем rbx
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rax, 0					
	mov rsi, 0 
	mov rcx, 0
	mov rdx, 0 
	mov r11, 10  ; кладем в регистр множитель 
.loop:
	mov sil, [rdi+rcx]  ; перемещаем символ
	cmp sil, '0' ; проверяем, что код символа больше кода 0
	jl .finish ; если нет - выход
	cmp sil, '9' ; проверяем, что код символа меньше кода 9
	jg .finish ; если нет - выход
	inc rcx
	sub sil, '0' ; переводим символ в число
	mul r11	; умножаем на 10
	add rax, rsi ; добавляем символ
	jmp .loop
.finish:
	mov rdx, rcx
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-' ; проверка на знак
    jne parse_uint ; >=0 печатаем
    inc rdi ; инкрементим индекс строки
    call parse_uint 
    neg rax ; меняем знак на +
    inc rdx ; инкрементим счетчик
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx ; обнуляем счетчик цикла
    .loop:
    cmp rcx, rdx  ; сравниваем счетчик и количество символов
    jz .fail   ; выход 
    mov r11, [rdi + rcx]  ; сохраняем в r11 символ
    mov [rsi + rcx], r11   ; копируем
    cmp r11, 0
    je .finish ; выход если это был последний символ  
    inc rcx  ; либо увеличиваем счетчик
    jmp .loop
.fail:
    mov rax, 0
 .finish:
    ret


